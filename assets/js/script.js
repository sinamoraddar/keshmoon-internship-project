$(document).ready(() => {
  let isToggleButtonCliked = false;
  let qualityCheckboxValues = [];
  let selectedItem;

  function qualityCheckbox(checkboxValue) {
    let value = checkboxValue.currentTarget.value;
    let valueIndex = qualityCheckboxValues.indexOf(value);
    console.log(qualityCheckboxValues);
    if (valueIndex === -1) {
      qualityCheckboxValues.push(value);
      $(checkboxValue.currentTarget.children[0])
        .removeClass("far fa-square")
        .addClass("fas fa-check-square");
    } else {
      qualityCheckboxValues.splice(valueIndex);
      $(checkboxValue.currentTarget.children[0])
        .removeClass("fas fa-check-square")
        .addClass("far fa-square");
    }
  }

  $(".mainsection__sideBar__section__title").click(function() {
    if (isToggleButtonCliked) {
      $("i", this).css({ transform: "rotate(0deg)" });
    } else {
      $("i", this).css({ transform: "rotate(-90deg)" });
    }
    isToggleButtonCliked = !isToggleButtonCliked;
    $(".mainsection__sideBar__section__list").slideToggle();
  });
  //   $(".mainsection__sideBar__section__list li").
  $(".mainsection__sideBar__section__item").click(function() {
    if (selectedItem) {
      $(selectedItem).removeClass("selectedItem");
      selectedItem = $(this);
      $(selectedItem).addClass("selectedItem");
      console.log(selectedItem);
    } else {
      selectedItem = $(this);
      $(selectedItem).addClass("selectedItem");
    }
  });
  //   qualitycheckbox event handler
  $(".qualityCheckBox").click(qualityCheckbox);

  // sortBy selector
  const sortByCriterion = { price: "price", popularity: "popularity" };
  let sortByValue = sortByCriterion.price;
  let isDropDownOpen = false;
  function sortBySelector(event) {
    sortByValue = event.currentTarget.value;
    if (isDropDownOpen) {
      $(".sortBy__type i").css({ transform: "rotate(0deg)" });
      isDropDownOpen = !isDropDownOpen;
    } else {
      $(".sortBy__type i").css({ transform: "rotate(-90deg)" });
      isDropDownOpen = !isDropDownOpen;
    }
    switch (sortByValue) {
      case sortByCriterion.price: {
        $(
          ".sortBy__type__selector button[value=" +
            sortByCriterion.popularity +
            "]"
        ).slideToggle();
        break;
      }
      case sortByCriterion.popularity: {
        $(
          ".sortBy__type__selector button[value=" + sortByCriterion.price + "]"
        ).slideToggle();
        break;
      }
    }
    console.log(sortByValue);
  }
  $(".sortBy__type__selector button[value=" + sortByValue + "]").show();
  $(".sortBy__type__selector button").click(sortBySelector);
});
